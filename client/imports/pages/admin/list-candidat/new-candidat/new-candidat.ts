import {Component, AfterContentInit} from '@angular/core';
import {Alert, AlertController, NavController, ViewController} from 'ionic-angular';
import {VerificationPage} from '../verification/verification';
import template from './new-candidat.html';
import {Meteor} from 'meteor/meteor'
import {ProfilePage} from "../profile/profile";
import {CandidatComponent} from "../candidat/candidat";
import {MeteorObservable} from "meteor-rxjs";
import {NewChatComponent} from "../../chats/new-chat";
import {LoginService} from "../../../../services/login";

@Component({
    template
})
export class NewCandidatComponent implements AfterContentInit {
    firstname = '';
    lastname = '';
    email = '';
    constructor(private alertCtrl: AlertController,
                private viewCtrl: ViewController,
                private loginService: LoginService) {
    }

    ngAfterContentInit() {

    }

    addCandidat(): void {
        this.handleAdd();
    }

    handleAdd(): void {
        this.loginService.create(this.firstname, this.lastname, this.email).then(() => {
            this.viewCtrl.dismiss();
        });
    }


    handleError(e: Error): void {
        console.error(e);

        const alert = this.alertCtrl.create({
            title: 'Oops!',
            message: e.message,
            buttons: ['OK']
        });

        alert.present();
    }
}