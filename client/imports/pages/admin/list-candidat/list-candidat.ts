import {Component, OnInit} from '@angular/core';
import {AlertController, ModalController, NavController, PopoverController, ViewController} from 'ionic-angular';
import {MeteorObservable} from 'meteor-rxjs';
import {Subscription, BehaviorSubject} from 'rxjs';
import template from './list-candidat.html';
import {Observable} from "rxjs/Rx";
import {NewCandidatComponent} from "./new-candidat/new-candidat";
import {ProfilePage} from "../profile/profile";
import {User} from "../../../../../imports/models";
import {Pictures, Users} from "../../../../../imports/collections";
import {NavComponent} from "../../nav/nav";

@Component({
    template
})
export class CandidatComponent implements OnInit {
    searchPattern: BehaviorSubject<any>;
    senderId: string;
    users: Observable<User[]>;
    usersSubscription: Subscription;

    constructor(private alertCtrl: AlertController,
                private modalCtrl: ModalController,
                private viewCtrl: ViewController,
                private popoverCtrl: PopoverController,
                private navCtrl: NavController,) {
        this.senderId = Meteor.userId();
        this.searchPattern = new BehaviorSubject(undefined);
    }

    ngOnInit(): void {
        this.observeSearchBar();
    }

    observeSearchBar(): void {
        this.searchPattern.asObservable()
        // Prevents the search bar from being spammed
            .debounce(() => Observable.timer(1000))
            .forEach(() => {
                if (this.usersSubscription) {
                    this.usersSubscription.unsubscribe();
                }

                this.usersSubscription = this.subscribeUsers();
            });
    }

    subscribeUsers(): Subscription {
        // Fetch all users matching search pattern
        const subscription = MeteorObservable.subscribe('users', this.searchPattern.getValue());
        const autorun = MeteorObservable.autorun();

        return Observable.merge(subscription, autorun).subscribe(() => {
            this.users = this.findUsers();
        });
    }

    findUsers(): Observable<User[]> {
        // Find all belonging chats
        return Users.find({roles: {$all: ['candidat']}
        })
            .startWith([]);
    }

    addCandidat(): void {
        const modal = this.modalCtrl.create(NewCandidatComponent);
        modal.present();
    }

    handleError(e: Error): void {
        console.error(e);

        const alert = this.alertCtrl.create({
            title: 'Oops!',
            message: e.message,
            buttons: ['OK']
        });

        alert.present();
    }

    getPic(pictureId): string {
        return Pictures.getPictureUrl(pictureId);
    }

    showCandidat(user) {console.log('SHOW');
        this.navCtrl.push(ProfilePage, {user: user});
    }

    showOptions(): void {
        const popover = this.popoverCtrl.create(NavComponent, {}, {
            cssClass: 'options-popover chats-options-popover'
        });

        popover.present();
    }
}