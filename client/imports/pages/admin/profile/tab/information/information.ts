import {Component, OnInit, ViewChild} from '@angular/core';
import {
    AlertController, Nav, NavController, NavParams, Platform, PopoverController, Navbar,
    ToastController
} from 'ionic-angular';
import {MeteorObservable} from 'meteor-rxjs';
import template from './information.html';
import {Dossierc, Profile, User} from "../../../../../../../imports/models";
import {PictureService} from "../../../../../services/picture";
import {CandidatComponent} from "../../../list-candidat/list-candidat";
import {Dossiercs, Pictures} from "../../../../../../../imports/collections";


@Component({
    template
})
export class InformationComponent implements OnInit {
    @ViewChild(Navbar) navBar: Navbar;
    picture: string;
    profile: Profile;
    user: User;
    modal: any;
    dossierc = <Dossierc>{};

    constructor(navParams: NavParams,
                private alertCtrl: AlertController,
                private navCtrl: Nav,
                private popoverCtrl: PopoverController,
                private pictureService: PictureService,
                public platform: Platform,
                private toastCtrl: ToastController) {
        this.user = navParams.data;
    }

    ionViewDidLoad() {
        this.navBar.backButtonClick = (e:UIEvent)=>{
            // todo something
            this.navCtrl.pop();
        }
    }

    ngOnInit(): void {

        this.profile = this.user.profile || {
            firstname: '',
            lastname: ''
        };

        MeteorObservable.subscribe('user').subscribe(() => {
            this.picture = Pictures.getPictureUrl(this.profile.pictureId);
        });

        MeteorObservable.subscribe('dossierc',
            this.user._id
        ).subscribe(() => {
            this.dossierc = Dossiercs.findOne({
                userId: this.user._id
            });

        });
    }

    selectProfilePicture(): void {
        this.pictureService.select().then((blob) => {
            console.log(blob);
            this.uploadProfilePicture(blob);
        })
            .catch((e) => {
                this.handleError(e);
            });
    }

    uploadProfilePicture(blob: Blob): void {
        console.log(blob);
        this.pictureService.upload(blob).then((picture) => {
            console.log(picture);
            this.profile.pictureId = picture._id;
            this.picture = picture.url;

        })
            .catch((e) => {
                this.handleError(e);
            });
    }

    handleSucess(message: String): void {

        let toast = this.toastCtrl.create({
            message: message,
            duration: 3000,
            position: 'top'
        });

        toast.present();
    }

    updateProfile(): void {
        MeteorObservable.call('updateProfile', this.user._id, this.profile).subscribe({
            next: () => {
                /*this.navCtrl.push(CandidatComponent);*/
                //this.navCtrl.setRoot(CandidatComponent);
            },
            error: (e: Error) => {
                this.handleError(e);
            }
        });
            console.log(this.dossierc);
            MeteorObservable.call('updateDossierc', this.dossierc).subscribe({
                next: () => {
                    this.handleSucess('Informations sauvergardées');
                },
                error: (e: Error) => {
                    if (e) {
                        this.handleError(e);
                    }
                }

            });
    }

    handleError(e: Error): void {
        console.error(e);

        const alert = this.alertCtrl.create({
            title: 'Oops!',
            message: e.message,
            buttons: ['OK']
        });

        alert.present();
    }

    backButtonAction(){
        console.log('BACK');
        /* checks if modal is open */
        if(this.modal && this.modal.index === 0) {
            /* closes modal */
            this.modal.dismiss();
        } else {
            /* exits the app, since this is the main/first tab */
            this.platform.exitApp();
            // this.navCtrl.setRoot(C);  <-- if you wanted to go to another page
        }
    }

    /*showOptions(): void {
        const popover = this.popoverCtrl.create(NavComponent, {}, {
            cssClass: 'options-popover chats-options-popover'
        });

        popover.present();
    }*/

    backProfile() {
        this.navCtrl.setRoot(CandidatComponent);
    }
}