import {Component, OnInit, ViewChild} from '@angular/core';
import {AlertController, Nav, NavController, NavParams, Platform, ToastController, Navbar} from 'ionic-angular';
import {MeteorObservable} from 'meteor-rxjs';
import template from './dossierc.html';
import {NavComponent} from "../../../nav/nav";
import {Dossierc, Profile, User} from "../../../../../../../imports/models";
import {Dossiercs} from "../../../../../../../imports/collections";
import { Http, Response } from '@angular/http';
import { Ng2DragDropModule } from 'ng2-drag-drop';
import { DragulaService } from 'ng2-dragula/ng2-dragula';



@Component({
    template,
    providers: [DragulaService]
})
export class DossiercComponent implements OnInit {

    @ViewChild(Navbar) navBar: Navbar;
    picture: string;
    profile: Profile;
    user: User;
    dossierc = <Dossierc>{};
    gender: string;
    private ArrListeEtoile: Object;
    private test: number;
    q1 = [];
    q2 = [];
    constructor(navParams: NavParams,
                private alertCtrl: AlertController,
                public platform: Platform,
                private toastCtrl: ToastController,
                private dragulaService: DragulaService) {
        dragulaService.setOptions('sixth-bag', {
            moves: function (el, container, handle) {
                return handle.className === 'handle';
            }
        });
        dragulaService.dropModel.subscribe((value) => {
            console.log(value);
            this.onDropModel(value.slice(1));
        });
        dragulaService.removeModel.subscribe((value) => {
            this.onRemoveModel(value.slice(1));
        });
        this.test = 1;
        this.gender = 'b';
        this.user = navParams.data;

        for (let i = 0; i < 20; i++) {
            this.q1.push("1. <" + i + ">");
            this.q2.push("2. <" + i + ">");
        }

        dragulaService.setOptions('third-bag', {
            removeOnSpill: false
        });

        dragulaService.drop.subscribe((value) => {
            console.log(value);
            let alert = this.alertCtrl.create({
                title: 'Item moved',
                subTitle: 'So much fun!',
                buttons: ['OK']
            });
            //alert.present();
        });

    }
    private onDropModel(args) {
        let [el, target, source] = args;
        console.log(args);
        // do something else
    }

    private onRemoveModel(args) {
        let [el, source] = args;
        console.log(args);
        // do something else
    }

    ngOnInit(): void {
        console.log(Ng2DragDropModule);
        this.ArrListeEtoile = new Object();

        this.profile = this.user.profile || {
            firstname: '',
            lastname: ''
        };

        MeteorObservable.subscribe('dossierc',
            this.user._id
        ).subscribe(() => {
            this.dossierc = Dossiercs.findOne({
                userId: this.user._id
            });

        });
        /*this.CreateListeEtoile('test', 4);*/
        

    }

    gestionHover(va, indice, nbEtoile, in55){
        for (let i=1; i<= nbEtoile; i++)
        {
            let idoff = "staroff-" + va + in55 +"-" + i;
            let idon = "staron-" + va + in55 +"-" + i;

            if(indice == -1)
            {
                // Sortie du survol de la liste des etoiles
                if (this.dossierc[va][in55].niveau >= i){
                    document.getElementById(idoff).style.display ="none";
                    document.getElementById(idon).style.display ="block";
                }
                else{
                    document.getElementById(idoff).style.display ="block";
                    document.getElementById(idon).style.display ="none";
                }
            }
            else
            {
                // Survol de la liste des etoiles
                if(i <= indice){
                    document.getElementById(idoff).style.display ="none";
                    document.getElementById(idon).style.display ="block";
                }
                else{
                    document.getElementById(idoff).style.display ="block";
                    document.getElementById(idon).style.display ="none";
                }
            }
        }
    }

    choixSelection(va, indice, nbEtoile){
        this.dossierc[va][nbEtoile].niveau = indice;

    }


    handleError(e: Error): void {
        const alert = this.alertCtrl.create({
            title: 'Oops!',
            message: e.message,
            buttons: ['OK']
        });

        alert.present();
    }

    handleSucess(message: String): void {
     
            let toast = this.toastCtrl.create({
                message: message,
                duration: 3000,
                position: 'top'
            });

            toast.present();
    }



    downloadPDF() {
        MeteorObservable.call('download', this.dossierc._id, this.user._id).subscribe({
            next: (data) => {
                var link = document.createElement("a");
                console.log(data);
                link.href = data;
                link.download = "DossierDe.pdf";
                link.click();
            },
            error: (e: Error) => {
                if (e) {
                    this.handleError(e);
                }
            }

        });
    }

    add(va, type) {
        type == 'object' ? this.dossierc[va].push({}) : this.dossierc[va].push('');
        if (va == 'techno' )
            this.dossierc.techno[this.dossierc.techno.length - 1].niveau = 0;
        else if (va == 'secteur' )
            this.dossierc.secteur[this.dossierc.secteur.length - 1].niveau = 0;
    }
    trackByIndex(index: number, obj: any): any {
        return index;
    }

    remove(va, index) {
        this.dossierc[va].splice(index, 1);
    }

    saveInfo() {
        console.log(this.dossierc);
        MeteorObservable.call('updateDossierc', this.dossierc).subscribe({
            next: () => {
                this.handleSucess('Informations sauvergardées');
            },
            error: (e: Error) => {
                if (e) {
                    this.handleError(e);
                }
            }

        });
    }

}