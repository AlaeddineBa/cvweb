import { Component, Injectable } from '@angular/core';
import { Alert, AlertController, NavController, ViewController, App } from 'ionic-angular';
import { LoginService } from '../../services/login';
import { LoginPage } from '../login/login';
import template from './nav.html';
import {ProfilePage} from "../admin/profile/profile";

@Component({
  template
})
@Injectable()
export class NavComponent {
  constructor(
    private alertCtrl: AlertController,
    private navCtrl: NavController,
    private loginService: LoginService,
    private viewCtrl: ViewController,
    public app: App
  ) {}

  editProfile(): void {
    this.viewCtrl.dismiss().then(() => {
      this.navCtrl.push(ProfilePage);
    });
  }

  logout(): void {
    const alert = this.alertCtrl.create({
      title: 'Logout',
      message: 'Are you sure you would like to proceed?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Yes',
          handler: () => {
            this.handleLogout(alert);
            return false;
          }
        }
      ]
    });

    this.viewCtrl.dismiss().then(() => {
      alert.present();
    });
  }

  handleLogout(alert: Alert): void {
    alert.dismiss().then(() => {
      return this.loginService.logout();
    })
    .then(() => {
      /*this.navCtrl.setRoot(LoginPage, {}, {
        animate: true
      });*/
        this.app.getRootNav().setRoot(LoginPage);
    })
    .catch((e) => {
      this.handleError(e);
    });
  }

  handleError(e: Error): void {
    console.error(e);

    const alert = this.alertCtrl.create({
      title: 'Oops!',
      message: e.message,
      buttons: ['OK']
    });

    alert.present();
  }
}