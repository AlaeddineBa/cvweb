import { Component, AfterContentInit } from '@angular/core';
import { Alert, AlertController, NavController } from 'ionic-angular';
import { LoginService } from '../../services/login';
import { VerificationPage } from '../verification/verification';
import template from './login.html';
import { Meteor } from 'meteor/meteor'
import {CandidatComponent} from "../admin/list-candidat/list-candidat";
import {CandidatPage} from "../candidat/candidat";
import { Roles } from 'meteor/alanning:roles';

@Component({
  template
})
export class LoginPage implements AfterContentInit {
    loginIn = '';
    passwordIn = '';

  constructor(
    private alertCtrl: AlertController,
    private phoneService: LoginService,
    private navCtrl: NavController,
    private loginService: LoginService
  ) {}

  ngAfterContentInit() {
    this.phoneService.getNumber().then((phone) => {
      if (phone) {
        this.login(phone);
      }
    });
  }

  onInputKeypress({keyCode}: KeyboardEvent): void {
    if (keyCode === 13) {
      this.login();
    }
  }

  login(loginIn: string = this.loginIn, passwordIn: string = this.passwordIn): void {

      this.handleLogin();
  }

  handleLogin(): void {

      this.loginService.login(this.loginIn, this.passwordIn).then(() => {
          console.log('ICI4');
          this.navCtrl.setRoot(Roles.userIsInRole( Meteor.user()._id, ['admin'] ) ? CandidatComponent : CandidatPage, {}, {
              animate: true
          });
      })

          .catch((e) => {
              this.handleError(e);
          });;

  }

  handleError(e: Error): void {
    console.error(e);

    const alert = this.alertCtrl.create({
      title: 'Oops!',
      message: 'Login ou mot de passe incorrect',
      buttons: ['OK']
    });

    alert.present();
  }
}