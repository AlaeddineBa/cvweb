import {Component, OnInit} from '@angular/core';
import {AlertController, NavParams, Platform, PopoverController, ToastController} from 'ionic-angular';
import {MeteorObservable} from 'meteor-rxjs';
import template from './information.html';
import {Profile, User} from "../../../../../../imports/models";
import {PictureService} from "../../../../services/picture";
import {Pictures} from "../../../../../../imports/collections";
import {NavComponent} from "../../../nav/nav";


@Component({
    template
})
export class InformationCandidatComponent implements OnInit {
    picture: string;
    profile: Profile;
    user: User;

    constructor(navParams: NavParams,
                private alertCtrl: AlertController,
                private popoverCtrl: PopoverController,
                private pictureService: PictureService,
                public platform: Platform,
                private toastCtrl: ToastController) {
        this.user = navParams.data;
    }


    ngOnInit(): void {

        this.profile = this.user.profile || {
            firstname: '',
            lastname: ''
        };

        MeteorObservable.subscribe('user').subscribe(() => {
            this.picture = Pictures.getPictureUrl(this.profile.pictureId);
        });
    }

    selectProfilePicture(): void {
        this.pictureService.select().then((blob) => {
            console.log(blob);
            this.uploadProfilePicture(blob);
        })
            .catch((e) => {
                this.handleError(e);
            });
    }

    uploadProfilePicture(blob: Blob): void {
        console.log(blob);
        this.pictureService.upload(blob).then((picture) => {
            console.log(picture);
            this.profile.pictureId = picture._id;
            this.picture = picture.url;

        })
            .catch((e) => {
                this.handleError(e);
            });
    }

    updateProfile(): void {
        MeteorObservable.call('updateProfile', this.user._id, this.profile).subscribe({
            next: () => {
                /*this.navCtrl.push(CandidatComponent);*/
                /*this.navCtrl.setRoot(CandidatComponent);*/
                this.handleSucess('Informations sauvergardées');
            },
            error: (e: Error) => {
                this.handleError(e);
            }
        });
    }

    handleError(e: Error): void {
        console.error(e);

        const alert = this.alertCtrl.create({
            title: 'Oops!',
            message: e.message,
            buttons: ['OK']
        });

        alert.present();
    }

    handleSucess(message: String): void {

        let toast = this.toastCtrl.create({
            message: message,
            duration: 3000,
            position: 'top'
        });

        toast.present();


    }


    showOptions(): void {
        const popover = this.popoverCtrl.create(NavComponent, {}, {
            cssClass: 'options-popover chats-options-popover'
        });

        popover.present();
    }
}