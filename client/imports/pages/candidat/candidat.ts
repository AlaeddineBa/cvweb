import {Component, OnInit, ViewChild} from '@angular/core';
import {AlertController, NavController, NavParams, PopoverController, Tabs} from 'ionic-angular';
import {MeteorObservable} from 'meteor-rxjs';
import { Meteor } from 'meteor/meteor';
import template from './candidat.html';
import {Observable, Subscriber} from "rxjs/Rx";
import {PictureService} from "../../services/picture";
import {InformationComponent} from "../admin/profile/tab/information/information";
import {DossiercComponent} from "../admin/profile/tab/dossier-competences/dossierc";
import {Chat, Message, Profile, User} from "../../../../imports/models";
import {Chats, Messages, Pictures, Users} from "../../../../imports/collections";
import {CandidatComponent} from "../admin/list-candidat/list-candidat";
import {MessagesPage} from "../admin/profile/tab/chat/messages";
import {ChatsPageCandidat} from "./tab/chats/chats";
import {NavComponent} from "../nav/nav";
import {InformationCandidatComponent} from "./tab/information/information";


@Component({
    template
})
export class CandidatPage implements OnInit {
    @ViewChild('myTabs') tabRef: Tabs;
    picture: string;
    profile: Profile;
    user: User;
    private tab1: InformationCandidatComponent;
    private tab2: ChatsPageCandidat;
    private tab4: DossiercComponent;
    private senderId: String;
    private chat: Chat;
    private selected: number;

    constructor(navParams: NavParams,
                private alertCtrl: AlertController,
                private navCtrl: NavController,
                private popoverCtrl: PopoverController,
                private pictureService: PictureService,) {
        this.user = Meteor.user();
        this.tab1 = InformationCandidatComponent;
        this.tab2 = ChatsPageCandidat;
        this.tab4 = DossiercComponent;
        this.senderId = Meteor.userId();
    }

    switchTabs() {

        if(this.tabRef.getSelected())
            this.selected = this.tabRef.getSelected().index;
        //console.log(this.tabRef.getSelected().index);
        console.log('AAAAA');
    }

    ngOnInit(): void {
        this.profile = this.user.profile || {
            firstname: '',
            lastname: ''
        };

        /*MeteorObservable.subscribe('chats').subscribe(() => {
            MeteorObservable.autorun().subscribe(() => {


                this.chat = Chats.findOne({memberIds: {$all: [this.senderId]}});

                if (!this.chat) {
                    //Meteor.call('addChat', this.user._id);
                    //this.chat = Chats.findOne({memberIds: {$all: [this.user._id, this.senderId]}});
                    MeteorObservable.call('addChat', this.user._id).subscribe((chat: Chat) => {
                        console.log(chat);
                        this.chat = chat;
                        this.chat.title = '';
                        this.chat.picture = '';

                        const receiverId = this.chat.memberIds.find(memberId => memberId !== this.senderId);
                        const receiver = Users.findOne(receiverId);

                        if (receiver) {
                            this.chat.title = receiver.profile.firstname + ' ' + receiver.profile.lastname;
                            this.chat.picture = Pictures.getPictureUrl(receiver.profile.pictureId);
                        }

                        // This will make the last message reactive
                        this.findLastChatMessage(this.chat._id).subscribe((message) => {
                            this.chat.lastMessage = message;
                        });
                    });
                }else{
                this.chat.title = '';
                this.chat.picture = '';

                const receiverId = this.chat.memberIds.find(memberId => memberId !== this.senderId);
                const receiver = Users.findOne(receiverId);

                if (receiver) {
                    this.chat.title = receiver.profile.firstname + ' ' + receiver.profile.lastname;
                    this.chat.picture = Pictures.getPictureUrl(receiver.profile.pictureId);
                }

                // This will make the last message reactive
                this.findLastChatMessage(this.chat._id).subscribe((message) => {
                    this.chat.lastMessage = message;
                });
            }
            });
        });*/

        MeteorObservable.subscribe('user').subscribe(() => {
            this.picture = Pictures.getPictureUrl(this.profile.pictureId);
        });
    }

    selectProfilePicture(): void {
        this.pictureService.select().then((blob) => {
            console.log(blob);
            this.uploadProfilePicture(blob);
        })
            .catch((e) => {
                this.handleError(e);
            });
    }

    findLastChatMessage(chatId: string): Observable<Message> {
        return Observable.create((observer: Subscriber<Message>) => {
            const chatExists = () => !!Chats.findOne(chatId);

            // Re-compute until chat is removed
            MeteorObservable.autorun().takeWhile(chatExists).subscribe(() => {
                Messages.find({ chatId }, {
                    sort: { createdAt: -1 }
                }).subscribe({
                    next: (messages) => {
                        // Invoke subscription with the last message found
                        if (!messages.length) {
                            return;
                        }

                        const lastMessage = messages[0];
                        observer.next(lastMessage);
                    },
                    error: (e) => {
                        observer.error(e);
                    },
                    complete: () => {
                        observer.complete();
                    }
                });
            });
        });
    }

    uploadProfilePicture(blob: Blob): void {
        console.log(blob);
        this.pictureService.upload(blob).then((picture) => {
            console.log(picture);
            this.profile.pictureId = picture._id;
            this.picture = picture.url;
          /*  MeteorObservable.call('updatePicture', this.user._id, this.profile).subscribe({
                next: () => {
                    /!*this.navCtrl.push(CandidatComponent);*!/
                },
                error: (e: Error) => {
                    console.log(e);
                }
            });*/
        })
            .catch((e) => {
                this.handleError(e);
            });
    }

    updateProfile(): void {
        console.log(this.profile);
        MeteorObservable.call('updateProfile', this.user._id, this.profile).subscribe({
            next: () => {
                this.navCtrl.push(CandidatComponent);
            },
            error: (e: Error) => {
                this.handleError(e);
            }
        });
    }

    handleError(e: Error): void {
        console.error(e);

        const alert = this.alertCtrl.create({
            title: 'Oops!',
            message: e.message,
            buttons: ['OK']
        });

        alert.present();
    }

    showOptions(): void {
        const popover = this.popoverCtrl.create(NavComponent, {}, {
            cssClass: 'options-popover chats-options-popover'
        });

        popover.present();
    }
}