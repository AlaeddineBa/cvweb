import {NgModule, ErrorHandler} from '@angular/core';
import {AgmCoreModule} from 'angular2-google-maps/core';
import {MomentModule} from 'angular2-moment';
import {IonicApp, IonicModule, IonicErrorHandler} from 'ionic-angular';
import {MyApp} from "./app.component";
import {ChatsPage} from "../pages/chats/chats";
import {MessagesPage} from "../pages/admin/profile/tab/chat/messages";
import {LoginPage} from "../pages/login/login";
import {ProfilePage} from "../pages/admin/profile/profile";
import {ChatsOptionsComponent} from "../pages/chats/chats-options";
import {NewChatComponent} from "../pages/chats/new-chat";
import {NavComponent} from "../pages/nav/nav";
import {NewCandidatComponent} from "../pages/admin/list-candidat/new-candidat/new-candidat";
import {CandidatComponent} from "../pages/admin/list-candidat/list-candidat";
import {DossiercComponent} from "../pages/admin/profile/tab/dossier-competences/dossierc";
import {InformationComponent} from "../pages/admin/profile/tab/information/information";
import {MessagesOptionsComponent} from "../pages/admin/profile/tab/chat/messages-options";
import {MessagesAttachmentsComponent} from "../pages/admin/profile/tab/chat/messages-attachments";
import {NewLocationMessageComponent} from "../pages/admin/profile/tab/chat/location-message";
import {ShowPictureComponent} from "../pages/admin/profile/tab/chat/show-picture";
import {LoginService} from "../services/login";
import {PictureService} from "../services/picture";
import {CandidatPage} from "../pages/candidat/candidat";
import {ChatsPageCandidat} from "../pages/candidat/tab/chats/chats";
import {MessagesCandidatPage} from "../pages/candidat/tab/chat/messages";
import {NewChatComponentCandidat} from "../pages/candidat/tab/chats/new-chat";
import {ChatsOptionsComponentCandidat} from "../pages/candidat/tab/chats/chats-options";
import {InformationCandidatComponent} from "../pages/candidat/tab/information/information";
import { DragulaModule } from 'ng2-dragula';


@NgModule({
    declarations: [
        MyApp,
        ChatsPage,
        MessagesPage,
        LoginPage,
        ProfilePage,
        ChatsOptionsComponent,
        NewChatComponent,
        NavComponent,
        NewCandidatComponent,
        CandidatComponent,
        DossiercComponent,
        InformationComponent,
        CandidatPage,
        NewChatComponentCandidat,
        ChatsOptionsComponentCandidat,
        InformationCandidatComponent,
        ChatsPageCandidat,
        MessagesCandidatPage,
        MessagesOptionsComponent,
        MessagesAttachmentsComponent,
        NewLocationMessageComponent,
        ShowPictureComponent
    ],
    imports: [
        IonicModule.forRoot(MyApp),
        MomentModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyAWoBdZHCNh5R-hB5S5ZZ2oeoYyfdDgniA'
        }),
        DragulaModule
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        ChatsPage,
        MessagesPage,
        LoginPage,
        ProfilePage,
        ChatsOptionsComponent,
        NewChatComponent,
        NavComponent,
        NewCandidatComponent,
        CandidatComponent,
        DossiercComponent,
        CandidatPage,
        NewChatComponentCandidat,
        ChatsOptionsComponentCandidat,
        ChatsPageCandidat,
        InformationComponent,
        MessagesCandidatPage,
        InformationCandidatComponent,
        MessagesOptionsComponent,
        MessagesAttachmentsComponent,
        NewLocationMessageComponent,
        ShowPictureComponent
    ],
    providers: [
        {provide: ErrorHandler, useClass: IonicErrorHandler},
        LoginService,
        PictureService
    ]
})
export class AppModule {
}