import { Component } from '@angular/core';
import {App, Platform, ViewController} from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';
import { Meteor } from 'meteor/meteor';
import template from "./app.html";
import { ChatsPage } from '../pages/chats/chats';
import {LoginPage} from "../pages/login/login";
import {CandidatComponent} from "../pages/admin/list-candidat/list-candidat";
import { Roles } from 'meteor/alanning:roles';
import {CandidatPage} from "../pages/candidat/candidat";
@Component({
  template
})
export class MyApp {
  rootPage: any;
 /*   private app: App;*/

  constructor(platform: Platform) {
      console.log(Meteor.user());
      //console.log(Roles.userIsInRole( Meteor.user()._id, ['admin'] ));
      this.rootPage = Meteor.user() ? Roles.userIsInRole( Meteor.user()._id, ['admin'] ) ? CandidatComponent : CandidatPage : LoginPage;
      //this.rootPage = Meteor.user() ? CandidatComponent : LoginPage;

      //this.rootPage = Meteor.user() ? CandidatComponent : LoginPage;

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      if (platform.is('cordova')) {
        StatusBar.styleDefault();
        Splashscreen.hide();
      }
    });

  /*    platform.registerBackButtonAction(() => {
          let nav = this.app.getActiveNav();
          let activeView: ViewController = nav.getActive();

          if(activeView != null){
              if(nav.canGoBack()) {
                  nav.pop();
              }else if (typeof activeView.instance.backButtonAction === 'function')
                  activeView.instance.backButtonAction();
              else nav.parent.select(0); // goes to the first tab
          }
      });*/

  }
}