import {Injectable} from '@angular/core';
import {Platform} from 'ionic-angular';
import {Sim} from 'ionic-native';
import {Accounts} from 'meteor/accounts-base';
import {Meteor} from 'meteor/meteor';
import {MeteorObservable} from "meteor-rxjs";
import {ChatsPage} from "../pages/chats/chats";

@Injectable()
export class LoginService {
    constructor(private platform: Platform) {

    }

    getNumber(): Promise<string> {
        if (!this.platform.is('cordova') ||
            !this.platform.is('mobile')) {
            return Promise.resolve('');
        }

        return Sim.getSimInfo().then((info) => {
            return '+' + info.phoneNumber;
        });
    }


    login(login: string, password: string): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            Meteor.loginWithPassword(login, password, (e: Error) => {
                if (e) {
                    return reject(e);
                }

                resolve();
            });
        });
    }

    create(firstname: string, lastname: string, email: string): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            /*Accounts.createUser({
                email: email,
                password: 'test',
                profile: {
                    firstname: firstname,
                    lastname: lastname
                }
            }, (e: Error) => {
                if (e) {
                    return reject(e);
                }

                resolve();
            });*/
            MeteorObservable.call('addCandidat', firstname, lastname, email).subscribe({
                next: () => {
                    resolve();
                },
                error: (e: Error) => {
                    reject(e);
                }
            });
        });
    }

    logout(): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            Meteor.logout((e: Error) => {
                if (e) {
                    return reject(e);
                }

                resolve();
            });
        });
    }
}