import { Meteor } from 'meteor/meteor';

export const DEFAULT_PICTURE_URL = '/assets/default-profile-pic.svg';

export interface Profile {
  firstname?: string;
  lastname?: string;
  picture?: string;
  pictureId?: string;
}


export enum MessageType {
  TEXT = <any>'text',
  LOCATION = <any>'location',
  PICTURE = <any>'picture'
}

export interface Chat {
  _id?: string;
  title?: string;
  picture?: string;
  lastMessage?: Message;
  memberIds?: string[];
}

export interface Message {
  _id?: string;
  chatId?: string;
  senderId?: string;
  content?: string;
  createdAt?: Date;
  ownership?: string;
  type?: MessageType;
}

export interface User extends Meteor.User {
  profile?: Profile;
}

export interface Location {
  lat: number;
  lng: number;
  zoom: number;
}



export interface Dossierc {
    _id?: string;
    titre: string;
    fi?: string[];
    formation?: string[];
    poste: string;
    presentation?: string;
    langue?: Langue[];
    techno?: Techno[];
    secteur?: Techno[];
    competence?: Competence[];
    experience?: Experience[];
    userId?: string;
    anneeEx?: string;
    user?: string;
}

export interface Langue {
    titre?: string;
    niveau?: string;
}

export interface Experience {
    date?: string;
    poste?: string;
    titre?: string;
    titre_poste?: string;
    co?: string;
}

export interface Techno {
    titre?: string;
    niveau?: number;
}

export interface Competence {
    titre?: string;
    niveau?: number;
}

export interface Picture {
  _id?: string;
  complete?: boolean;
  extension?: string;
  name?: string;
  progress?: number;
  size?: number;
  store?: string;
  token?: string;
  type?: string;
  uploadedAt?: Date;
  uploading?: boolean;
  url?: string;
  userId?: string;
}
