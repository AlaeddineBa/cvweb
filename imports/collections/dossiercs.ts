import {MongoObservable} from "meteor-rxjs";
import {Dossierc} from "../models";

export const Dossiercs = new MongoObservable.Collection<Dossierc>('dossierc');
