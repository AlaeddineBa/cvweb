import {Meteor} from 'meteor/meteor';
import {Chats, Messages, Dossiercs, Users} from '../imports/collections';
import {Dossierc, MessageType, Profile} from '../imports/models';
import {check, Match} from 'meteor/check';
import {Email} from 'meteor/email'
import {Roles} from 'meteor/alanning:roles';

;

const nonEmptyString = Match.Where((str) => {
    check(str, String);
    return str.length > 0;
});

Meteor.methods({
    addChat(receiverId: string) {
        if (!this.userId) {
            throw new Meteor.Error('unauthorized',
                'User must be logged-in');
        }

        check(receiverId, nonEmptyString);

        if (receiverId === this.userId) {
            throw new Meteor.Error('illegal-receiver',
                'Receiver must be different than the current logged in user');
        }

        const chatExists = !!Chats.collection.find({
            memberIds: {$all: [this.userId, receiverId]}
        }).count();

        if (chatExists) {
            throw new Meteor.Error('chat-exists',
                'Chat already exists');
        }

        const chat = {
            memberIds: [this.userId, receiverId]
        };
        let chatId;
        chatId = Chats.collection.insert(chat);
        return Chats.findOne({_id: chatId});
    },

    removeChat(chatId: string): void {
        if (!this.userId) {
            throw new Meteor.Error('unauthorized',
                'User must be logged-in to remove chat');
        }

        check(chatId, nonEmptyString);

        const chatExists = !!Chats.collection.find(chatId).count();

        if (!chatExists) {
            throw new Meteor.Error('chat-not-exists',
                'Chat doesn\'t exist');
        }

        Chats.remove(chatId);
    },

    updateProfile(id: String, profile: Profile): void {
        if (!this.userId) throw new Meteor.Error('unauthorized',
            'User must be logged-in to');
        check(id, nonEmptyString);
        check(profile, {
            firstname: nonEmptyString,
            lastname: nonEmptyString,
            pictureId: Match.Maybe(nonEmptyString)
        });

        Meteor.users.update(id, {
            $set: {profile}
        });
    },

    addMessage(type: MessageType, chatId: string, content: string) {
        if (!this.userId) throw new Meteor.Error('unauthorized',
            'User must be logged-in');

        check(type, Match.OneOf(String, [MessageType.TEXT, MessageType.LOCATION]));
        check(chatId, nonEmptyString);
        check(content, nonEmptyString);

        const chatExists = !!Chats.collection.find(chatId).count();

        if (!chatExists) {
            throw new Meteor.Error('chat-not-exists',
                'Chat doesn\'t exist');
        }

        return {
            messageId: Messages.collection.insert({
                chatId: chatId,
                senderId: this.userId,
                content: content,
                createdAt: new Date(),
                type: type
            })
        };
    },
    download: function (id: String, userId: String) {

        let doae = Dossiercs.findOne({
            _id: id
        });

        let user = Users.findOne({_id: userId});
        /*console.log(doae);*/

        doae.user = user.profile.firstname.charAt(0) + user.profile.lastname.charAt(0);
        let response = HTTP.call("POST", 'http://localhost:8888/test.php', {
            data: {data: JSON.stringify(doae)}
        });
        console.log(response.content);
        if (response.error) {
            console.log(response.error);
        }
        return response.content;
    },
    addCandidat(firstname: string, lastname: string, email: string) {
        if (!this.userId) throw new Meteor.Error('unauthorized',
            'User must be logged-in');
        check(firstname, nonEmptyString);
        check(lastname, nonEmptyString);
        check(email, nonEmptyString);
        const randomstring = Math.random().toString(36).slice(-10);
        let userId = Accounts.createUser({
            email: email,
            password: randomstring,
            profile: {
                firstname: firstname,
                lastname: lastname
            }
        });
        console.log('Password: ' + randomstring);
        Roles.addUsersToRoles(userId, 'candidat');
        this.unblock();
        Email.send({
            from: 'alaeddine.baghdadi1@gmail.com',
            to: email,
            subject: 'Creation d un compte',
            text: 'login: ' + email + '\n\n' + 'Password: ' + randomstring
        });
        Dossiercs.collection.insert({
            titre: '',
            fi: [],
            formation: [],
            presentation: '',
            langue: [],
            poste: '',
            competence: [],
            techno: [],
            secteur: [],
            experience: [],
            userId: userId
        });
    },
    countMessages(): number {
        return Messages.collection.find().count();
    },
    updateDossierc(dossierc: Dossierc) {
        Dossiercs.update(dossierc._id, dossierc);
    }
});